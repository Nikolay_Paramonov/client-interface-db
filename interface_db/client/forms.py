from django import forms


class ClientsFilterForm(forms.Form):
    first_name = forms.CharField(label='Имя', required=False)
    last_name = forms.CharField(label='Фамилия', required=False)
    ordering = forms.ChoiceField(label='сортировка', required=False, choices=[
        ['-id', 'по умолчанию'],
        ['first_name', 'по имени'],
        ['last_name', 'по фамилии'],
        ['date_of_birth', 'по дате рождения'],
    ])
