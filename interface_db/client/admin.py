from django.contrib import admin
from .models import Clients


@admin.register(Clients)
class AdminClient(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'date_of_birth']
    exclude = ('score_vote',)
