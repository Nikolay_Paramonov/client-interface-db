from django.urls import path
from rest_framework.routers import DefaultRouter
from . import views
from .views import ClientViewSet

router = DefaultRouter()
router.register(r'api/list', ClientViewSet)


urlpatterns = [
    path('', views.client_list, name='client_list'),
    path('client/<int:pk>/', views.client_detail, name='client_detail'),
    path('edit-client/', views.ClientCreateView.as_view(), name='edit_client'),
    path('delete-client/<int:pk>/', views.ClientDeleteView.as_view(), name='delete_client'),
    path('export-excel', views.export_clients_xlsx, name='export_excel'),
    path('index/', views.index, name='index'),
    path('api/vote/<int:pk>', views.ClientApiView.as_view()),

]


urlpatterns += router.urls
