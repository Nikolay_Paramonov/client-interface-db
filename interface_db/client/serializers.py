from .models import Clients
from rest_framework import serializers


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clients
        fields = ['score_vote']


class ClientSerializerViewSet(serializers.ModelSerializer):
    class Meta:
        model = Clients
        fields = ['id', 'first_name', 'last_name', 'date_of_birth', 'photo', 'score_vote']
