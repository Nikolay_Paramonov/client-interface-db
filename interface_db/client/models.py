from django.db import models
from datetime import date
from django.core.validators import MaxValueValidator


class Clients(models.Model):
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    date_of_birth = models.DateField(verbose_name='Дата рождения')
    photo = models.ImageField(blank=True, null=True, upload_to='images/', verbose_name='Фото')
    score_vote = models.IntegerField(default=0, validators=[MaxValueValidator(10)])

    def age(self):
        tod = date.today()
        try:
            dob = self.date_of_birth.replace(year=tod.year)
        except ValueError:
            dob = self.date_of_birth.replace(year=tod.year, month=self.date_of_birth.mounth + 1, day=1)

        if dob > tod:
            return tod.year - self.date_of_birth.year - 1
        else:
            return tod.year - self.date_of_birth.year

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ['-id']

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)
