from django.db import transaction
from django.shortcuts import render, get_object_or_404
from rest_framework.viewsets import ModelViewSet
from .models import Clients
from .forms import ClientsFilterForm
from django.views.generic.edit import CreateView, DeleteView
from django.urls import reverse_lazy
import xlwt
from django.http import HttpResponse
from rest_framework.views import APIView
from .serializers import ClientSerializer, ClientSerializerViewSet
from rest_framework.response import Response
from django.db.models import F


def client_list(request):
    clients = Clients.objects.all()
    form = ClientsFilterForm(request.GET)
    if form.is_valid():
        if form.cleaned_data['first_name']:
            clients = clients.filter(first_name__icontains=form.cleaned_data['first_name'])
        if form.cleaned_data['last_name']:
            clients = clients.filter(last_name__icontains=form.cleaned_data['last_name'])
        if form.cleaned_data['ordering']:
            clients = clients.order_by(form.cleaned_data['ordering'])
    return render(request, 'client/client_list.html', {'clients': clients, 'form': form})


def client_detail(request, pk):
    client = get_object_or_404(Clients, pk=pk)
    return render(request, 'client/client_detail.html', {'client': client})


class ClientCreateView(CreateView):
    model = Clients
    fields = ['first_name', 'last_name', 'date_of_birth', 'photo']
    template_name = 'client/edit_client.html'
    success_url = reverse_lazy('edit_client')


class ClientDeleteView(DeleteView):
    model = Clients
    fields = '__all__'
    template_name = 'client/delete_client.html'
    success_url = reverse_lazy('client_list')


def export_clients_xlsx(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = "attachment; filename=DB Clients.xlsx"

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Client Data')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['id', 'First Name', 'Last Name', 'Date of birth', 'Photo']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    rows = Clients.objects.all().values_list('id', 'first_name', 'last_name', 'date_of_birth', 'photo')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, str(row[col_num]), font_style)

    wb.save(response)

    return response


def index(request):
    return render(request, 'client/index.html')


class ClientViewSet(ModelViewSet):
    queryset = Clients.objects.all()
    serializer_class = ClientSerializerViewSet


class ClientApiView(APIView):
    @transaction.atomic
    def post(self, request, pk):
        client = Clients.objects.select_for_update().get(pk=pk)
        serializer = ClientSerializer(client)
        rsp = {'score_vote': client.score_vote, 'answer': 'Ваш голос не учтён, уже 10 голосов!'}
        if client.score_vote < 10:
            client.score_vote = F('score_vote') + 1
            client.save()
            client.refresh_from_db()
            return Response(serializer.data)
        return Response(rsp)
