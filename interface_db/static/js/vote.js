const URL = "http://127.0.0.1:8000";
const app = new Vue({
    el: "#app",
    data: {
        clients: [],
    },

    methods: {
        voteClient(client, index) {
           axios
              .post(URL + '/api/vote/' + client.id)
              .then(response => {
              this.clients[index].score_vote = response.data.score_vote;
              this.clients[index].answer = response.data.answer;
           })
              .catch(response => {
              console.log(error);
      })
        }

    },
    created() {
        axios
        .get(URL + "/api/list/")
        .then(response => {
        this.clients = response.data;
        })
        .catch(response => {
            console.log(error);
      })
    },
    template:
     `<ul>
     <div v-for="(client, index) in clients":key="index">
         <img :src="client.photo" display: flex width=10%>
         <br>
         <div v-if="client.score_vote < 10">
            <button @click="voteClient(client, index)">+1</button>
         </div>
         <p>{{ client.answer }}</p>
        <p>Голосов: {{ client.score_vote }} из 10</p>
     </div>
     </ul>`
});
